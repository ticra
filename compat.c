#include "config.h"
#include "compat.h"

#include <stdarg.h>
#include <stdio.h>

RCSID("$Id: compat.c,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $");

#ifdef NEED_SETPROCTITLE
void
setproctitle(const char *fmt, ...)
{
}
#endif

#ifdef NEED_SNPRINTF
int
snprintf(char *str, size_t size, const char *fmt, ...)
{
	va_list ap;
	int rc;

	va_start(ap, fmt);
	rc = vsprintf(str, fmt, ap);
	va_end(ap);

	return rc;
}
#endif

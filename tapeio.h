/* $Id: tapeio.h,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $ */

#ifndef TAPEIO_H
#	define TAPEIO_H

#define HEADERSIZE 8192

typedef enum { STARTMARK, STOPMARK, FILEMARK } filetype_t;

struct tapelabel {
	char labelstr[MAXLINE];
	time_t date;
};
typedef struct tapelabel tapelabel_t;

struct fileheader {
	char host[MAXLINE];
	char vol[MAXLINE];
	filetype_t type;
	time_t date;
};
typedef struct fileheader fileheader_t;

/* Access primitives. */
int mt_rewind(int fd);
int mt_fsf(int fd, int count);
int mt_weof(int fd, int count);

int readlabel(int fd, tapelabel_t *label);
int writelabel(int fd, tapelabel_t *label);
int readheader(int fd, fileheader_t *header);
int writeheader(int fd, fileheader_t *header);

#endif

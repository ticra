#include "config.h"
#include "err.h"
#include "strutil.h"
#include "tapeio.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mtio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <time.h>
#include <unistd.h>

#ifdef NEED_STRINGS
#include <strings.h>
#endif

RCSID("$Id: tapeio.c,v 1.2 2000/02/22 22:50:33 shmit Exp $");

int
mt_rewind(int fd)
{
	struct mtop mt;

	mt.mt_op = MTREW;
	mt.mt_count = 1;
	return ioctl(fd, MTIOCTOP, &mt);
}

int
mt_fsf(int fd, int count)
{
	struct mtop mt;

	mt.mt_op = MTFSF;
	mt.mt_count = count;
	return ioctl(fd, MTIOCTOP, &mt);
}

int
mt_weof(int fd, int count)
{
	struct mtop mt;

	mt.mt_op = MTWEOF;
	mt.mt_count = count;
	return ioctl(fd, MTIOCTOP, &mt);
}

int
readlabel(int fd, tapelabel_t *label)
{
	char buffer[HEADERSIZE], date[MAXLINE];
	char *p;
	struct tm tm;
	ssize_t readb;

	do {
		readb = read(fd, buffer, sizeof(buffer));
		if (readb == -1) {
			err("Taper: couldn't read label from tape: %s.\n",
			    strerror(errno));
			return -1;
		}
	} while (readb);

	p = getword(buffer, label->labelstr);
	getword(p, date);
	sscanf(date, "%02d%02d%04d", &tm.tm_mday, &tm.tm_mon, &tm.tm_year);
	tm.tm_mon--; tm.tm_year -= 1900; tm.tm_isdst = -1;

	label->date = mktime(&tm);

	if (mt_fsf(fd, 0) == -1) {
		err("Taper: couldn't fast-forward past label: %s.\n",
		    strerror(errno));
		return -1;
	}

	return 0;
}

int
writelabel(int fd, tapelabel_t *label)
{
	char buffer[HEADERSIZE];
	struct tm *tm;
	int wroteb;

	tm = localtime(&label->date);
	snprintf(buffer, sizeof(buffer), "%s %02d%02d%04d", label->labelstr,
		 tm->tm_mday, tm->tm_mon+1, tm->tm_year+1900);

	wroteb = write(fd, buffer, sizeof(buffer));
	if (wroteb == -1)
		return -1;

	return mt_weof(fd, 1);
}

int
readheader(int fd, fileheader_t *header)
{
	char buffer[HEADERSIZE];
	ssize_t readb;

	do {
		readb = read(fd, buffer, sizeof(buffer));
		if (readb == -1)
			return -1;
	} while (readb);

	bzero(header, sizeof(header));
	if (!strncmp(TAPER_START, buffer, sizeof(buffer)))
		header->type = STARTMARK;
	else if (!strncmp(TAPER_STOP, buffer, sizeof(buffer)))
		header->type = STOPMARK;
	else {
		char *vol;
		struct tm tm;

		header->type = FILEMARK;
		vol = index(buffer, ':');
		if (vol) {
			*vol = '\0';
			sscanf(vol+1, "%s %02d%02d%04d", header->vol,
			       &tm.tm_mday, &tm.tm_mon, &tm.tm_year);
			tm.tm_mon--; tm.tm_year -= 1900; tm.tm_isdst = -1;
			header->date = mktime(&tm);
		}
		strncpy(header->host, buffer, sizeof(header->host));
	}

	return mt_fsf(fd, 0);
}

int
writeheader(int fd, fileheader_t *header)
{
	char buffer[HEADERSIZE];
	struct tm *tm;
	ssize_t wroteb;

	switch (header->type) {
	case STARTMARK:
		strncpy(buffer, TAPER_START, sizeof(buffer));
		break;
	case STOPMARK:
		strncpy(buffer, TAPER_STOP, sizeof(buffer));
		break;
	case FILEMARK:
		tm = localtime(&header->date);
		snprintf(buffer, sizeof(buffer), "%s:%s %02d%02d%04d",
			 header->host, header->vol,
			 tm->tm_mday, tm->tm_mon+1, tm->tm_year+1900);
		break;
	}

	wroteb = write(fd, buffer, sizeof(buffer));
	if (wroteb == -1)
		return -1;

	return mt_weof(fd, 1);
}

#!/bin/sh

# $Id: report.sh,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $

manager=`getconf manager`
logdir=`getconf logdir`
infolog=`getconf infolog`
errorlog=`getconf errorlog`

if [ "$infolog" != "-" ]; then
	Mail -s "backup info report" "$manager" < "$logdir/$infolog"
	rm -f "$logdir/$infolog"
fi

if [ "$errorlog" !=  "-"  -a -s "$logdir/$errorlog" ]; then
	Mail -s "backup error report" "$manager" < "$logdir/$errorlog"
	rm -f "$logdir/$errorlog"
fi

/* $Id: err.h,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $ */

#ifndef ERR_H
#	define ERR_H

void err(const char *fmt, ...);
void readerr(int infd, const char *host);

void initlog(const char *infon, const char *errorn);
void log_err(const char *fmt, ...);
void log_info(const char *fmt, ...);

#endif

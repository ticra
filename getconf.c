#include "config.h"
#include "conf.h"

#include <stdio.h>

RCSID("$Id: getconf.c,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $");

int
main(int argc, char *argv[])
{
	if (read_config(SERVER_CONFIG_FILE) == -1)
		return 1;

	while (--argc) {
		const config_t *option;

		option = findopt(argv[argc]);
		if (!option)
			fprintf(stderr,
				"Error: option `%s' doesn't exist or"
				" is not set.\n", argv[argc]);
		else
			switch (option->type) {
			case NUMOPT:
				printf("%ld\n", option->numvalue);
				break;
			case STROPT:
				printf("%s\n", option->strvalue);
				break;
			default:
				fprintf(stderr,
					"Error: don't understand option type"
					" (this shouldn't happen).\n");
			}
	}

	return 0;
}

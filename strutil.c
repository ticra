#include "config.h"
#include "strutil.h"

#include <ctype.h>
#include <string.h>

RCSID("$Id: strutil.c,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $");

char *
getword(const char *src, char *dst)
{
	int inquote = 0;

	while (isspace(*src))
		src++;

	while (*src) {
		if (*src == '\\' && *++src)
			*dst++ = *src++;
		else if (*src == '"') {
			if (inquote)
				inquote = 0;
			else
				inquote = 1;
			src++;
		} else if (*src == '#') {
			if (!inquote)
				break;
			*dst++ = *src++;
		} else if (!inquote && isspace(*src))
			break;
		else
			*dst++ = *src++;
	}
	*dst = '\0';

	if (!*src || *src == '#')
		return NULL;

	return (char *)src;
}

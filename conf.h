/* $Id: conf.h,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $ */

#ifndef CONF_H
#	define CONF_H

/* Option names. */
#define MANAGER		"manager"
#define LOGDIR		"logdir"
#define INFOLOG		"infolog"
#define ERRORLOG	"errorlog"
#define HOSTLIST	"hostlist"
#define TIMEOUT		"timeout"
#define RETRIES		"retries"
#define SPOOLDIR	"spooldir"
#define SPOOLSIZE	"spoolsize"
#define TAPEDEV		"tapedev"
#define TAPESIZE	"tapesize"
#define LABELSTR	"labelstr"
#define PORTOPT		"port"
#define DUMPOPT		"dump"
#define TAROPT		"tar"
#define SIZEOPT		"size"
#define SIZE_RE		"size_re"

/* Compression names. */
#define DC_UNCOMP	"uncompressed"
#define DC_COMP		"compressed"
#define DC_COMP9	"comp-best"
#define DC_COMPFAST	"comp-fast"

/* Auth names. */
#define DA_NONE		"noauth"
#define DA_RSH		"rsh"

typedef enum { STROPT, NUMOPT } opt_t;
typedef enum { UNCOMP, COMP, COMP9, COMPFAST } comp_t;
typedef enum { NOAUTH, RSH } auth_t;

struct _config_t {
	char *name;
	opt_t type;
	char *strvalue;
	long numvalue;
};
typedef struct _config_t config_t;

struct _dump_t {
	char d_name[BUFFSIZE];
	char d_cmdline[MAXPATHLEN];
	char d_estline[MAXPATHLEN];
	char d_regexp[BUFFSIZE];
};
typedef struct _dump_t dump_t;

struct _disk_t {
	char vol[MAXPATHLEN];
	const dump_t *type;
	comp_t comp;
	auth_t auth;
};
typedef struct _disk_t disk_t;

struct _disklist_t {
	disk_t disk;
	struct _disklist_t *next;
};
typedef struct _disklist_t disklist_t;

const config_t *findopt(const char *name);
int read_config(const char *filen);

const dump_t *find_dumptype(const char *name);
int read_dumptypes(const char *filen);

disklist_t *read_disklist(const char *filen);

#endif

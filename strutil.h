/* $Id: strutil.h,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $ */

#ifndef STRUTIL_H
#	define STRUTIL_H

char *getword(const char *src, char *dst);

#endif

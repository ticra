#include "config.h"
#include "conf.h"
#include "lock.h"
#include "tapeio.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

RCSID("$Id: label.c,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $");

char *progname;

void
usage()
{
	fprintf(stderr, "Usage: %s label\n", progname);
}

int
main(int argc, char *argv[])
{
	const config_t *option;
	tapelabel_t label;
	int fd;

	if (read_config(SERVER_CONFIG_FILE) == -1) {
		perror("Error: couldn't read config file");
		return 1;
	}

	option = findopt(LABELSTR);
	if (!option) {
		fprintf(stderr, "Error: " LABELSTR " option isn't set.\n");
		return 1;
	}

	if (memcmp(argv[1], option->strvalue, strlen(option->strvalue)))
		fprintf(stderr,
			"Warning: desired label doesn't match "
			"labelstring option.\n");

	option = findopt(TAPEDEV);
	if (!option) {
		fprintf(stderr, "Error: " TAPEDEV " option isn't set.\n");
		return 1;
	}

	fd = open(option->strvalue, O_WRONLY);
	if (fd == -1) {
		perror("Error: couldn't open tape device");
		return 1;
	}

	printf("Rewinding device...");
	fflush(stdout);
	if (mt_rewind(fd) == -1) {
		perror("Error: couldn't rewind tape device");
		close(fd);
		return 1;
	}

	printf("writing label `%s'...", argv[1]);
	fflush(stdout);

	strncpy(label.labelstr, argv[1], sizeof(label.labelstr));
	label.date = time(NULL);
	if (writelabel(fd, &label) == -1) {
		close(fd);
		return 1;
	}

	if (mt_weof(fd, 1) == -1) {
		perror("Error: couldn't write EOF marker");
		close(fd);
		return 1;
	}

	printf("rewinding device...");
	fflush(stdout);
	if (mt_rewind(fd) == -1) {
		perror("Error: couldn't rewind tape device");
		close(fd);
		return 1;
	}

	printf("done.\n");
	close(fd);
	return 0;
}

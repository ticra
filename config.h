/* $Id: config.h,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $ */

#ifndef CONFIG_H
#	define CONFIG_H

#include <limits.h>
#include <sys/param.h>
#include <sys/time.h>

#include "compat.h"

#ifndef PREFIX
#define PREFIX "/home/shmit/src/ticra"
#endif

#ifndef BINDIR
#define BINDIR PREFIX
#endif

#ifndef LIBEXECDIR
#define LIBEXECDIR PREFIX
#endif

#ifndef DATADIR
#define DATADIR PREFIX
#endif

/* Path to place debugging files. */
#define TMPPATH "/tmp"

/* Default config file to load. */
#define SERVER_CONFIG_FILE DATADIR "/server.conf"
#define CLIENT_CONFIG_FILE DATADIR "/client.conf"

/* Path to rsh or replacement. Should probably go into config file. */
#define PATH_RSH "/usr/local/bin/ssh"

/* If random syscalls fail with EAGAIN, how many times to try. */
#define MAXRETRIES 10

/* --- You probably don't want to edit any of the below. --- */

/*
 * Dumper client/server protocol below, edit at your own risk.
 */

/* Initialization protocol. */
#define INIT_REQ	"WHAT DO YOU WANT"
#define INIT_ACK	"INFORMATION"

/* Single transaction protocol. */
#define REQ_ACK		"ACK"
#define REQ_NACK	"NACK"

#define PORT_REQ	"PORT"
#define DUMP_SENDME	"SENDME"
#define DUMP_DONE	"DONE"
#define DUMP_ERROR	"ERROR"

/* Disk request fields. */
#define DISK_REQ	"IHAVE"
#define DISK_NAME	"NAME"
#define DISK_AUTH	"AUTH"
#define DISK_SIZE	"SIZE"
#define DISK_LEVEL	"LEVEL"

/* Auth request field. */
#define AUTH_NOAUTH	"NONE"
#define AUTH_RSH	"RSH"

/*
 * Taper protocol below, same as above.
 */
#define TAPER_START	"YOU WONT GET IT"
#define TAPER_STOP	"BY HOOK OR BY CROOK, WE WILL"

/*
 * OS macros/defines below.
 */
#define RCSID(x) static const char rcsid[] __attribute__((__unused__)) = x

/* Buffer size to use to dump to tape. */
#define BUFFSIZE 8192

#ifndef MAXLINE
#define MAXLINE 256
#endif

#ifndef MAXPATHLEN
#define MAXPATHLEN 128
#endif

#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 256
#endif

#ifndef FD_SETSIZE
#define FD_SETSIZE 256
#endif
#endif

/* $Id: compat.h,v 1.2 2000/02/22 22:50:33 shmit Exp $ */

#ifndef COMPAT_H
#	define COMPAT_H

#include <sys/types.h>

#ifdef __NetBSD__
#define sockopt_t int
#endif

#ifdef __FreeBSD__
#define NEED_LIBUTIL
#define sockopt_t int
#endif

#ifdef __svr4__
#define NEED_STRINGS
#define NEED_SETPROCTITLE
#define NEED_SNPRINTF
#define sockopt_t int
#endif

#ifdef __linux__
#define NEED_SETPROCTITLE
#define sockopt_t char
#endif

#ifdef NEED_SETPROCTITLE
void setproctitle(const char *fmt, ...);
#endif

#ifdef NEED_SNPRINTF
int snprintf(char *str, size_t size, const char *fmt, ...);
#endif

#endif

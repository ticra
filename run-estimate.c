#include "config.h"
#include "conf.h"
#include "err.h"

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef NEED_LIBUTIL
#include <libutil.h>
#endif

RCSID("$Id: run-estimate.c,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $");

short exit_status = -1;

void
sig_pipe(int signo)
{
	err("SIGPIPE caught.\n");
	exit_status = -2;
}

void
sig_child(int signo)
{
	int status;

	if (wait(&status) == -1) {
		err("Couldn't clean up child: %s.\n", strerror(errno));
		return;
	}

	exit_status = WEXITSTATUS(status);
}

int
init_connection(disklist_t *disklist, long port)
{
	char buffer[MAXLINE];

	printf(PORT_REQ " %ld\n", port);
	fflush(stdout);

	if (fgets(buffer, sizeof(buffer), stdin) == NULL) {
		err("NULL input waiting for ACK.\n");
		return -1;
	}

	if (!strcmp(buffer, REQ_NACK "\n")) {
		err("OK. I'm bailing.\n");
		return -1;
	} else if (strcmp(buffer, REQ_ACK "\n")) {
		err("Don't know what `%s' means, giving up.\n", buffer);
		return -1;
	}

	while (disklist) {
		char authtype[MAXLINE];

		switch (disklist->disk.auth) {
		case RSH:
			strcpy(authtype, AUTH_RSH);
			break;
		case NOAUTH:
		default:
			strcpy(authtype, AUTH_NOAUTH);
		}

		printf("%s %s %s %s %s\n", DISK_REQ,
		       DISK_NAME, disklist->disk.vol,
		       DISK_AUTH, authtype);

		fflush(stdout);

		disklist = disklist->next;

		if (fgets(buffer, sizeof(buffer), stdin) == NULL) {
			err("NULL input waiting for ACK.\n");
			return -1;
		}

		if (!strcmp(buffer, REQ_NACK "\n"))
			/* TODO: remove item from disklist in this event. */
			continue;
		else if (strcmp(buffer, REQ_ACK "\n")) {
			err("Don't know what `%s' means, giving up.\n",
			     buffer);
			return -1;
		}
	}

	printf(DUMP_DONE "\n");
	return 0;
}

int
main(int argc, char *argv[])
{
	char buffer[MAXLINE];
	const config_t *option;
	disklist_t *disklist;
	long port;

	if (read_config(CLIENT_CONFIG_FILE) == -1)
		return 1;

	option = findopt(PORTOPT);
	if (!option) {
		err("Couldn't find " PORTOPT " in config file.\n");
		return 1;
	}
	port = option->numvalue;

	printf(INIT_REQ "\n");
	fflush(stdout);

	if (fgets(buffer, sizeof(buffer), stdin) == NULL) {
		err("NULL input waiting for handshake ACK.\n");
		return 1;
	}

	if (strncmp(buffer, INIT_ACK "\n", sizeof(buffer))) {
		err("Recieved invalid handshake ACK: %s.", buffer);
		return 1;
	}

	if (read_dumptypes(DATADIR "/dumptypes") == -1)
		return 1;

	disklist = read_disklist(DATADIR "/disklist");
	if (!disklist)
		return 1;

	if (init_connection(disklist, port) == -1)
		return 1;

	return 0;
}

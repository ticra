#include "config.h"
#include "err.h"

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#ifdef NEED_STRINGS
#include <strings.h>
#endif

RCSID("$Id: err.c,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $");

FILE *errfile = NULL;
FILE *infofile = NULL;

void
err(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	printf(DUMP_ERROR "\n");
	fflush(stdout);
	vfprintf(stderr, fmt, ap);
	fflush(stderr);
	va_end(ap);
}

/* TODO: use stream reading instead of raw mode. */
void
readerr(int infd, const char *host)
{
	static char buffer[MAXLINE];
	char *i, *p;
	static ssize_t leftover = 0;
	ssize_t readb;

	while ((readb = read(infd, buffer+leftover,
			     sizeof(buffer)-leftover)) > 0) {
		p = buffer;
		while ((i = index(p, '\n'))) {
			*i = '\0';

			log_err("%s: %s\n", host, p);
			p = i+1;
		}

		if (p-buffer < readb) {
			leftover = readb-(p-buffer);
			memmove(buffer, p, leftover);

			/* TODO: dynamically allocate in this case. */
			if (leftover == sizeof(buffer)) {
				fprintf(stderr, "DEBUG: really big line.\n");
				buffer[sizeof(buffer)] = '\0';
				log_err("%s: %s\n", host, buffer);
				leftover = 0;
			}
		}
	}
}

void
initlog(const char *infon, const char *errorn)
{
	const char *i;

	i = rindex(infon, '/');
	if (i)
		i++;
	else
		i = infon;
	if (!strcmp(i, "-"))
		infofile = stdout;
	else {
		unlink(infon);
		infofile = fopen(infon, "w");
		if (!infofile) {
			fprintf(stderr,
				"Error: couldn't write log file `%s',"
			        " using stderr: %s.\n", infon, strerror(errno));
			infofile = stdout;
		}
	}

	i = rindex(errorn, '/');
	if (i)
		i++;
	else
		i = errorn;
	if (!strcmp(i, "-"))
		errfile = stderr;
	else {
		unlink(errorn);
		errfile = fopen(errorn, "w");
		if (!errfile) {
			fprintf(stderr,
				"Error: couldn't write log file `%s',"
			        " using stderr: %s.\n",
				errorn, strerror(errno));
			errfile = stderr;
		}
	}
}

void
log_info(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(infofile, fmt, ap);
	va_end(ap);
}

void
log_err(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(errfile, fmt, ap);
	va_end(ap);
}

/* $Id: lock.h,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $ */

#ifndef LOCK_H
#	define LOCK_H

int lock_create(const char *name);
void lock_delete(const char *name);

#endif

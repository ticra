# $Id: Makefile,v 1.1.1.1 1999/02/02 23:29:39 shmit Exp $

PREFIX=/home/shmit
#PREFIX=/usr/local/ticra
BINDIR=$(PREFIX)/bin
LIBEXECDIR=$(PREFIX)/libexec/ticra
DATADIR=$(PREFIX)/libdata/ticra

CC=gcc
INSTALL=install

BINMODE=755
DATAMODE=644

# This is needed for FreeBSD machines.
OSLIBS=-lutil

# SysV Machines.
#OSLIBS=-lnsl -lsocket

PATHFLAGS=-DBINDIR=\"$(BINDIR)\" -DLIBEXECDIR=\"$(LIBEXECDIR)\"		\
	  -DDATADIR=\"$(DATADIR)\"
COPTS=-Wall -g -O3 -pipe -DDEBUG

CFLAGS=$(COPTS) $(PATHFLAGS)
LIBS=$(OSLIBS)

# Source files.
SOURCES=dumper.c run-dump.c taper.c label.c conf.c strutil.c err.c compat.c \
	smrsh.c lock.c

# Programs to build.
CLIENT_BIN=smrsh
CLIENT_LIBEXEC=run-dump run-estimate
SERVER_BIN=dumper restore label getconf
SERVER_LIBEXEC=taper report

# Examples files to install.
EXAMPLES=client.conf server.conf disklist hostlist dumptypes

.c.o:
	$(CC) $(CFLAGS) -c $<
.sh:
	cp -p $< $@
	chmod 755 $@

all:	server client tags

server:	$(SERVER_BIN) $(SERVER_LIBEXEC)

client:	$(CLIENT_BIN) $(CLIENT_LIBEXEC)

tags:	$(SOURCES)
	ctags $(SOURCES)

install: install-server install-client install-examples

install-server: server
	mkdir -p $(BINDIR)
	mkdir -p $(LIBEXECDIR)
	$(INSTALL) -c -m $(BINMODE) $(SERVER_BIN) $(BINDIR)
	$(INSTALL) -c -m $(BINMODE) $(SERVER_LIBEXEC) $(LIBEXECDIR)

install-client: client
	mkdir -p $(BINDIR)
	mkdir -p $(LIBEXECDIR)
	$(INSTALL) -c -m $(BINMODE) $(CLIENT_BIN) $(BINDIR)
	$(INSTALL) -c -m $(BINMODE) $(CLIENT_LIBEXEC) $(LIBEXECDIR)

install-examples:
	mkdir -p $(DATADIR)/examples
	$(INSTALL) -c -m $(DATAMODE) $(EXAMPLES) $(DATADIR)/examples

clean:
	rm -f $(SERVER_BIN) $(SERVER_LIBEXEC) $(CLIENT_BIN) $(CLIENT_LIBEXEC) \
	      tags *.o

#
# Binary dependencies.
#
DUMPER_OBJS=dumper.o conf.o lock.o strutil.o err.o compat.o
dumper:	$(DUMPER_OBJS)
	$(CC) $(CFLAGS) -o $@ $(DUMPER_OBJS) $(LIBS)

RUN-DUMP_OBJS=run-dump.o conf.o strutil.o err.o compat.o
run-dump: $(RUN-DUMP_OBJS)
	$(CC) $(CFLAGS) -o $@ $(RUN-DUMP_OBJS) $(LIBS)

RUN-ESTIMATE_OBJS=run-estimate.o conf.o strutil.o err.o
run-estimate: $(RUN-ESTIMATE_OBJS)
	$(CC) $(CFLAGS) -o $@ $(RUN-ESTIMATE_OBJS)

TAPER_OBJS=taper.o tapeio.o conf.o lock.o strutil.o err.o compat.o
taper:	$(TAPER_OBJS)
	$(CC) $(CFLAGS) -o $@ $(TAPER_OBJS) $(LIBS)

RESTORE_OBJS=restore.o tapeio.o conf.o lock.o strutil.o compat.o
restore: $(RESTORE_OBJS)
	$(CC) $(CFLAGS) -o $@ $(RESTORE_OBJS)

LABEL_OBJS=label.o tapeio.o conf.o lock.o strutil.o err.o compat.o
label:	$(LABEL_OBJS)
	$(CC) $(CFLAGS) -o $@ $(LABEL_OBJS)

GETCONF_OBJS=getconf.o conf.o strutil.o err.o
getconf: $(GETCONF_OBJS)
	$(CC) $(CFLAGS) -o $@ $(GETCONF_OBJS)

SMRSH_OBJS=smrsh.o
smrsh:	$(SMRSH_OBJS)
	$(CC) $(CFLAGS) -o $@ $(SMRSH_OBJS)

#
# Dependency rules.
#
dumper.o:	conf.h config.h err.h lock.h

run-dump.o:	conf.h strutil.h config.h

run-estimate.o:	conf.h config.h err.h

taper.o:	tapeio.h conf.h config.h err.h lock.h

restore.o:	tapeio.h conf.h config.h lock.h

label.o:	tapeio.h conf.h config.h lock.h

getconf.o:	conf.h config.h

smrsh.o:	config.h

tapeio.o:	tapeio.h err.h strutil.h

conf.o:		conf.h strutil.h config.h err.h

err.o:		err.h config.h

lock.o:		lock.h config.h

strutil.o:	strutil.h config.h

compat.o:	compat.h config.h

config.h:	compat.h Makefile
